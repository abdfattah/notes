
# Pengenalan Docker

::: warning
Setakat ini setup docker hanya dibuat pada ubuntu server, Linode VPS. Untuk windows belum nak buat lagi sebab takut berat
:::

## Apa itu Docker

Docker ni macam vagrant, dulu-dulu guna vagrant. Docker container tu untuk ship application secara pantas kerana setup dah dibuat pada container, ringkasnya begini (bagi sistem yang menggunakan PHP, NGINX, MARIADB):

- setup main os, cthnya guna VPS ubuntu server
- install docker
- tambah container PHP pada docker
- tambah container NGINX pada docker
- tambah container MARIADB pada docker
- dah boleh run sistem

setiap container memang dah configure siap-siap untuk run sistem tu. jadi jika container tu crash, hanya remove dan tambah container baru. masa downtime jadi lebih singkat.

fail-fail sistem juga boleh diletak pada main os, hanya mount kepada container, termasuklah fail fizikal mariadb. jadi backup boleh dibuat pada main os.

dengan container juga, boleh buat cluster dengan mudah sebab boleh run multiple container.

begitu juga replication mariadb boleh dibuat dengan mudah.

## Pemasangan

Kena buang old version (kalau ada)

```sh
$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

apt update dan install beberapa package untuk membenarkan pemasangan secara HTTPS

```sh
$ sudo apt-get update
$ sudo apt-get install ca-certificates curl gnupg lsb-release
```

Tambah docker GPG key

```sh
$ sudo mkdir -p /etc/apt/keyrings
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

Setup repository

```sh
$ echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Kemaskini pakej apt dan pasang versi terkini Docker Engine, containerd dan Docker Compose, atau boleh pasang versi docker yang tertentu (rujuk official docker docs - link di bawah):

```sh
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

Verify installation

```sh
$ sudo service docker start
$ sudo docker run hello-world
```

kod di atas akan start service docker dan run container hello world. nak check docker yang tengah run

```sh
$ sudo docker ps
```

nak check apa image docker yang telah didownload

```sh
$ sudo docker images
```

dale pikiran ambo, kalu guna rclone untuk regularly backup mysql file, ok dop?
ada dua kaedah, satu guna rclone backup folder data mysql, atau dump sql file. tp tok live data, cthnya kalu crash kul 3, dan last backup kul 2.30, ada 30 minit data hok tok backup.
satu cara lagi adalah buat slave master database dale dua docker. hok ni jadi tak dok miss data, semua backup pada slave

https://mfyz.com/using-rclone-cronjobs-for-simple-server-backup-solution/

cat /etc/os-release - untuk tgk os release

ujikaji supaya gabungkan php mysql nginx dalam satu docker file menggunakan base nginx container : GAGAL. kerana container tu OS lain dah, ada dua option di sini,
- lain-lain container : follow tutorial https://www.digitalocean.com/community/tutorials/how-to-set-up-laravel-nginx-and-mysql-with-docker-compose-on-ubuntu-20-04
- buat container sendiri based on ubuntu 22.04

::: tip Rujukan
- [Official Docker](https://docs.docker.com/engine/install/ubuntu/)
- [Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04)
https://blog.devsense.com/2019/php-nginx-docker
:::