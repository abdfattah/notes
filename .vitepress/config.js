export default {
    lang: 'en-US',
    title: 'Mubee',
    description: 'Notes',

    lastUpdated: true,
    cleanUrls: 'without-subfolders',

    markdown: {
        headers: {
            level: [0, 0]
        }
    },

    themeConfig: {
        nav: nav(),

        sidebar: {
            '/laravel/': sidebarLaravel(),
            '/mariadb/': sidebarMariaDB()
        },

        socialLinks: [
            { icon: 'github', link: 'https://github.com/abdulfattah/' }
        ],

        footer: {
            message: 'Ask a.fattah@ymail.com before use this notes',
            copyright: 'Copyright © 2022-present Abd Fattah Ibrahim'
        },

        algolia: {
            appId: 'PKK1EAJG8G',
            apiKey: '788afabaf322af4a28d3763c5e21a289',
            indexName: 'my_notes'
        },

        lastUpdatedText: 'Dikemaskini Pada'
    }
}

function nav() {
    return [
        {
            text: 'Nota-Nota',
            items: [
                {
                    text: 'Docker',
                    link: '/docker/'
                },
                {
                    text: 'Laravel',
                    link: '/laravel/'
                },
                {
                    text: 'MariaDB',
                    link: '/mariadb/'
                },
                {
                    text: 'NuxtJS',
                    link: '/nuxtjs/'
                },
                {
                    text: 'PHP',
                    link: '/php/'
                },
                {
                    text: 'Quasar',
                    link: '/quasar/'
                },
                {
                    text: 'Restic & RClone',
                    link: '/restic-rclone/'
                },
                {
                    text: 'Teknologi',
                    link: '/tech-service/'
                },
                {
                    text: 'Ubuntu',
                    link: '/ubuntu/'
                }
            ]
        }
    ]
}

function sidebarLaravel() {
    return [
        {
            text: 'Pemasangan',
            collapsible: true,
            items: [
                { text: 'Windows', link: '/laravel/' }
            ]
        }
    ]
}

function sidebarMariaDB() {
    return [
        {
            text: 'Pemasangan',
            items: [
                { text: 'Windows', link: '/mariadb/' }
            ]
        }
    ]
}
