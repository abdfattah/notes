---
layout: home

title: Mubee
titleTemplate: Nota-Nota

hero:
  name: Mubee
  tagline: Nota-nota pembangunan perisian
  actions:
    - theme: alt
      text: Docker
      link: /docker/
    - theme: alt
      text: Laravel
      link: /laravel/
    - theme: alt
      text: MariaDB
      link: /mariadb/
    - theme: alt
      text: NuxtJS
      link: /nuxtjs/
    - theme: alt
      text: PHP
      link: /php/
    - theme: alt
      text: Quasar
      link: /quasar/
    - theme: alt
      text: Restic & RClone
      link: /restic-rclone/
    - theme: alt
      text: Teknologi
      link: /tech-service/
    - theme: alt
      text: Ubuntu
      link: /ubuntu/
---
